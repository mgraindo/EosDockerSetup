#
# Simple EOS Docker file
#
# Version 0.2

#------------------------------------------------

#FROM scratch
#ADD centos-7-docker.tar.xz /

#LABEL name="CentOS Base Image" \
#    vendor="CentOS" \
#    license="GPLv2" \
#    build-date="20170705"

#CMD ["/bin/bash"]

#----------------------------------------------------

FROM centos:7
MAINTAINER Elvin Sindrilaru, esindril@cern.ch, CERN 2017

RUN yum -y --nogpg update

# Add required repositories
ADD *.repo /etc/yum.repos.d/

# Add configuration files for EOS instance
ADD eos.sysconfig /etc/sysconfig/eos
ADD xrd.cf.* /etc/
ADD krb5.conf /etc/

# Instal XRootD
ENV XRD_VERSION 4.5.0
RUN yum -y --nogpg install \
    xrootd-$XRD_VERSION \
    xrootd-client-$XRD_VERSION \
    xrootd-client-libs-$XRD_VERSION \
    xrootd-libs-$XRD_VERSION \
    xrootd-server-devel-$XRD_VERSION \
    xrootd-server-libs-$XRD_VERSION

RUN yum -y install heimdal-server heimdal-workstation krb5-workstation

# Install EOS
RUN yum -y --nogpg install\
    eos-server eos-testkeytab quarkdb\

    initscripts less emacs && yum clean all

ADD eos_setup.sh /
ADD eos_mq_setup.sh /
ADD eos_mgm_setup.sh /
ADD eos_fst_setup.sh /
ADD eos_mgm_fs_setup.sh /
ADD kdc.sh /
ENTRYPOINT ["/bin/bash"]

ADD scripts/*.sh /scripts/

# Install some package for the filesystems setup

RUN yum -y install e2fsprogs
RUN yum -y install xfsprogs
RUN yum -y install vim
RUN yum -y install epel-release
RUN yum -y install ShellCheck

