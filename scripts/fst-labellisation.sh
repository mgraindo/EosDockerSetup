#!/bin/bash


######################################
#     Filesystem labellisation       #
######################################


#The device name is taken as an argument : type ./script DeviceName (the device name must be written without /dev/)


#function which prevent the user that there is a problem and quit the program. It take the message which is written as an argument.

fatalError () {
        echo "Error : $1" >> stderr
        exit 1
}

#--------------------------------------------------------------------------------------------------------

#Variables
device=$1

needPartitionning=true

#--------------------------------------------------------------------------------------------------------

#Automatic generation of labels.

if [[ -e labelNumbers.txt ]]
then

	# Check which label is free from the numbers file created in the main
	sort -n labelNumbers.txt > labelNumbersSorted.txt #The max number is now on the last line of the file

	#Deletion of empty lines in the labelNumbers.txt file
	sed -i '/^$/d' labelNumbersSorted.txt

	#Recovery of the max number
	max=$(tail -n 1 labelNumbersSorted.txt) >/dev/null 2>&1

	# Check if there is some free label numbers before the max one : if the number of labels is equal to the max, there is not empty label. Else, we can take one of them.
	lines=$(wc -l labelNumbersSorted.txt | awk '{ print $1 }')

	if [ "$lines" -eq "$max" ]
	then

	        #No empty label before the last one
	        let "newLabelPostfixNumber = 10#$max + 1"

	        #Creation of the new label
	        if [ "$newLabelPostfixNumber" -le 9 ]
	        then
	                newLabelPostfix="0"$newLabelPostfixNumber
	        else
	                newLabelPostfix=$newLabelPostfixNumber
	        fi

	else

	        #Loop to see if there is a free label before the last one

	        ##Define the variables which will be needed in the loop

	        let "tmp = 1"
	        newLabelPostfix="0"$tmp
	
	        ##Loop
	        while read line && [[ "$newLabelPostfix" = "$line" ]]
	        do
	                let "tmp = $tmp + 1"
	                if [ "$tmp" -le 9 ]
	                then
	                        newLabelPostfix="0"$tmp
	                else
	                        newLabelPostfix=$tmp
	                fi
        	done < labelNumbersSorted.txt

	fi
else
	newLabelPostfix="01"
fi

newLabel="/data"$newLabelPostfix


echo "The label of the filesystem $device will be : $newLabel" >> stderr

#The device still needs to be mounted with its label.   
if [ -f toMount.txt ]
then
	cond1=$(grep "$newLabel" toMount.txt)
	cond2=$(grep "$device" toMount.txt)
        if [ -z "$cond1" ]  && [ -z "$cond2" ]
        then
                echo "$device $newLabel $needPartitionning" >> toMount.txt
        else
                fatalError "Something went wrong. Please try again"

        fi
else
        echo "$device $newLabel $needPartitionning" >> toMount.txt
fi

echo "$newLabelPostfix" >> labelNumbers.txt

