#!/bin/bash

#Use : ./fst-partitionning-and-mounting <device> <label> <needPartitionning> <opt_register>

device=$1
label=$2
needPartitionning=$3
opt_register=$4

#-----------------------------------------------------------------------------

#function which prevent the user that there is a problem and quit the program. It take the message which is written as an argument.

fatalError () {
        echo "Error : $1" >> stderr
        exit 1
}


##Execute the command or stop the program if it fails
noisily () {
        local cmd="$*"
        echo "    $cmd" >> stderr
        eval "$cmd" || fatalError "command failed: $cmd"
}
 
##Execute the command or print an error message if it fails
less_noisily () {
        local cmd="$*"
        echo "  $cmd" >> stderr
        eval "$cmd" || fatalError "command failed: $cmd"
}
 
##Execute the command without saying if it succeeded or failed
quietly () {
        local cmd="$*"
        echo "  $cmd" >> stderr
        eval "$cmd" || echo ""
}

cmd_timeout() {  

	timeout=$1   
	shift   
	cmd=$*   
	t=0   
	exit=0   
	$cmd &   
	pid=$!   

	while [[ $t -lt $timeout ]] && [ $exit -eq 0 ] ; do     
		sleep 1     
		(( t = t + 1 ))     
		proc=$(ps -ef | awk '{print $2}' | grep "$pid" )     
		if [[ -z "$proc" ]] ; then       
			(( exit = 1 ))     
		fi   
	done   

	if [[ $exit -ne 1 ]] ; then     
		kill -9 "$pid" 	
		echo "TIMEOUT - process killed" >&2   
	fi 

}

#-----------------------------------------------------------------------------

#########################
#  Filesystem Creation  #
#########################

#Partitionning if needed
                
if $needPartitionning
then

	mkfs.xfs -L "$label" "$device" >> stderr 2>&1
	if [ $? -ne 0 ]
        then
                fatalError "The system met a problem while partitionning the device $device "
        fi
        

        # Check if the partitionning is well done : The type of the partition must be "xfs". No need to check if the label exists, it was done before.

        i=$(blkid "$device" | awk '{print $4}')

        if echo "$i" | grep -q 'TYPE=\"xfs\"'
        then
        	echo "the filesystem $device is ready to be mounted" >> stderr
        else
        	fatalError "Something went wrong during the partitionning. The program will stop now, please try again."
        fi

fi

#------------------------------------------------------------------------------

##############
#  Mounting  #
##############

#Check if a directory named with this label already exists, and if it can be a mountpoint
if [ -d "$label" ]
then
        directoryContent=$(ls -A "$label")
        if [ -n "$directoryContent" ]
        then
                fatalError "A non empty directory $label was already created. Please clean this directory and try again"
        fi
else
        #Creation of the mouting directory if it doesn't exist
        mkdir "$label"
        if [ $? -ne 0 ]
        then
                fatalError "The system was not able to create the mountpoint directory"
        fi
fi

#Mounting
mount -t xfs "$device" "$label" >> stderr 2>&1
if [ $? -ne 0 ]
then
        fatalError "The mounting failed"
else 
	echo "The device $device has been well-mounted. The mountpoint is $label" >> stderr
fi

#The mountpoint should be mounted at this point
less_noisily "chown daemon:daemon $label"

if $opt_register
then

        ###################################
        #         Registering             # 
        ###################################

        eval "cmd_timeout 180 /usr/sbin/eosfstregister $label spare:1" || echo "ERROR! Command cmd_timeout 180 /usr/sbin/eosfstregister $label spare:1 failed but continue script"
	sleep 5

        #Check if eosfsid is assigned

        if [[ -e "$label/.eosfsuuid" ]]
        then
                echo "FSUUID assigned to $label"
                ls -la "$label"/.eosfsuuid
                ls -la "$label"/.eosfsid
        fi

fi

exit 0
