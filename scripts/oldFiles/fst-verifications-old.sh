#!/bin/bash


############################################
#     Check on the state of the device     #
############################################

#Fonctions which be used in the code

##Print an error message taken as argument and exit the program
fatalError () {
        echo "Error : $1" >> stderr
        exit 1
}

##Execute the command or stop the program if it fails
noisily () {
        local cmd="$*"
        echo "    $cmd" >>stderr
        eval "$cmd" || die "command failed: $cmd"
}

##Execute the command or print an error message if it fails
less_noisily () {
        local cmd="$*"
        echo "  $cmd" >> stderr
        eval "$cmd" || fatalError "command failed: $cmd"
}

##Execute the command without saying if it succeeded or failed
quietly () {
        local cmd="$*"
        echo "  $cmd" >> stderr
        eval "$cmd" || echo ""
}

#--------------------------------------------------------------------------------------------------------

#Variables and default options

device=$1"1"

opt_force=$2
needPartitionning=$3

#--------------------------------------------------------------------------------------------------------

echo "######################################" >> stderr
echo "# Preparation of the device /dev/$device #" >> stderr
echo "######################################" >> stderr

#--------------------------------------------------------------------------------------------------------

#Avoid the filesystem to be verified twice
echo $device >> verified.txt

#--------------------------------------------------------------------------------------------------------

#Check if the filesystem is still mounted

if mount | grep -q "/dev/$device"
then
        if $opt_force
        then
                #Unmount filesystems
                umount /dev/$device
                if [ $? -ne 0 ]
                then
                        fatalError "The unmounting of the filesystem $device failed"
                fi
        else
                fatalError "The data filesystem $device is already mounted, use the --force option to unmount"
        fi
fi



#------------------------------------------------------------------------------------------------------

# Access to the filesystem label
label=$( xfs_admin -l $device | sed 's/label = "\(.*\)"$/\1/' )


#Check the previous file system

if [[ $(dd if=$device bs=4 count=1 2>/dev/null) = XFSB ]] # XFS magic number, ie this file system is a xfs one.
then
	#Check old data

	## Creation of a temporary mountpoint to check if the filesystem is related to EOS
	somewhere="/tmp/test_old_fs_$RANDOM"
	echo "investigating old fs $device - mounting in $somewhere" >> stderr

	less_noisily "mkdir -p $somewhere" 	
	less_noisily "/bin/mount $device $somewhere"

	## Check if it's mounted
	present=$(/bin/mount | grep "$somewhere")
	if [[ -n "$present" ]]	#The filesystem is mounted
	then
		# Check inside if EOS related
		if [[ -e "$somewhere/.eosfsuuid" ]]
		then
			#The filesystem contain EOS data, it just has to be mounted
			echo "FS containing EOS data, keep it safe" >> stderr
			needPartitionning=false
			echo "$device $label $needPartitionning" >> toMount.txt
		else 
			#The filesystem doesn't contain any EOS data for now, let's see if the label of this filesystem match the expected ones. If it does, mount it.

			echo "FS non EOS related for now"
			needPartitionning=true

			#Check the label
 
			##Check if the label looks like "/data". If it's not the case, it stops the program.
			if echo "$label" | grep -E '^/data' >/dev/null 2>&1
			then
			        echo "The device /dev/$device already has a label, which is $label" >> stderr

			        ##Check if the number is valid
			        labelLength=${#label}
			        if [[ $labelLength -lt 7 ]]
			        then
			                fatalError "The device /dev/$device's label ($label) doesn't seems to match"
			        fi
 
			        ##Check if this label is not already used by a mounted device
			        mountlist=$(mount)
			        if echo "$mountlist" | grep "$label" >/dev/null 2>&1
			        then
			                fatalError "The label $label doesn't seems to be available"
			        fi
 
			        ##Once everything's okay, the device still needs to be mounted with its label.   
			        if [ -f toMount.txt ]
			        then
			                cond1=$(grep $device toMount.txt)
			                cond2=$(grep $label toMount.txt)
			                if [[ -n $cond1  || -n $cond2 ]]
			                then
			                        fatalError "Something went wrong. Please try again"
			                else
			                        echo "$device $label $needPartitionning" >> toMount.txt
			                fi
			        else
			                echo "$device $label $needPartitionning" >> toMount.txt
			        fi
 

		fi
		#Unmount the filesystem of the temporary mountpoint
		less_noisily "/bin/umount -f $somewhere"

		#Delete the temporary mountpoint
		rmdir "$somewhere"



else # The filesystem is not a xfs filesystem, and need mkfs.xfs

	if blkid /dev/$device | grep "TYPE=" >> stderr
	then
		fatalError "The filesystem can't be used in EOS"
	else
		needPartitionning=true
		./fst-labellisation.sh $device
        	if [ $? -ne 0 ]
        	then
        	        fatalError "The labellisation of the device /dev/$device failed"
        	fi
	fi
fi

