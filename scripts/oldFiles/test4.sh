#!/bin/bash

device=$1


# Check the state of the device

# New variable : a number which describe the state of the system
## 1 : Doesn't have a partition yet (partitionment needed) 
## 2 : Partition ready to be mounted

systemState="0"

        #Check if the device has already been partitioned, and if its type is "xfs"
 
if echo blkid | grep '/dev/$device' >/dev/null 2>&1
then
        i=$(blkid /dev/$device | awk '{print $4}')
        if echo "$i" | grep 'TYPE=\"xfs\"' >/dev/null 2>&1
        then
                echo "Your filesystem is ready to be mounted"
                systemState="2"
        else
                fatalError "The device /dev/$device is not a xfs partition."
        fi
else 
        echo "Your system needs to be partitionned."
        systemState="1"
fi


        #If the system is already partitionned :

case $systemState in




        #If the system is already partitionned :

        "2")
                #Check if the device is the one contening the OS

                for dev in $( /bin/df | grep " /$\| /boot$\| /var$" | awk '{print $1}' | tr -d '1234567890' | uniq )
                do
                        if [[ $dev = $device ]]; then
                                FatalError "You are trying to wipe, format and register the ROOT filesystem, are you really sure??"
                        fi
                done

                #Check if there is an available label

                i=$(blkid /dev/$device | awk '{print $2}')
                if echo "$i" | grep "LABEL=" >/dev/null 2>&1
                then
                        #Access to the existing label
                        label=$(blkid /dev/$device | awk '{print $2}' | sed 's/.\{7\}//'| sed -e 's/.\{1\}$//')

                        #Check if the label looks like "/data". If it's not the case, it stops the program.
                        if echo "$label" | grep "/data" </dev/null 2>&1
                        then
                                echo "This device already has a label, which is $label"
                        else
                                echo "This filesystem already has a label, but it doesn't seems to be an available label. To avoid any bad manipulation, the program will stop now."
                                exit
                        fi

                else
                        fatalError "Your device seems to be partitionned, but no label has been found."

                fi

        ;;

esac
