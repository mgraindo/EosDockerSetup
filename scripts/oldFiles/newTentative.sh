#!/bin/bash

device=$1

blkid /dev/$device | grep "PTTYPE=" >> stderr
if [ $? -eq 0 ]	#This device is a table of partitions
then

	filesystem=$device"1"

	#Check if the device is not already in toMount

	grep $filesystem toMount.txt >> stderr
	if [ $? -ne 0 ]
	then

		#Check if it's already partitionned
		blkid /dev/$filesystem | grep " TYPE=" >> stderr
		if [ $? -eq 0 ]
		then
			#Check if it's a xfs partition
			xfs_admin -l /dev/$filesystem >>stderr 2>&1

			if [ $? -eq 0 ] #The device is a xfs partition and has a label
			then
				#Check if the label is available
	                        label=$(xfs_admin -l /dev/$filesystem | sed 's/.\{9\}//'| sed -e 's/.\{1\}$//')	 

				#Check if the label looks like "/data". If it's not the case, it stops the program.
	                        if echo "$label" | grep -E '^/data' >/dev/null 2>&1
        	                then

	                                echo "The device /dev/$device already has a label, which is $label" >> stderr

        	                        #Check if the number is valid
        	                        labelLength=${#label}
        	                        if [[ $labelLength -lt 7 ]]
        	                        then
        	                                fatalError "The device /dev/$device's label ($label) doesn't seems to match"
       	                         	fi
	
	                                #Check if this label is not already used by a mounted device
	                                mountlist=$(mount)
	                                if echo "$mountlist" | grep "$label" >/dev/null 2>&1
	                                then
	                                        fatalError "The label $label doesn't seems to be available"
	                                fi

	                                #The device still needs to be mounted with its label.   
	                                if [ -f toMount.txt ]
	                                then
	                                        cond1=$(grep $device toMount.txt)
	                                        cond2=$(grep $label toMount.txt)
	                                        if [[ -n $cond1  || -n $cond2 ]]
	                                        then
	                                                fatalError "Something went wrong. Please try again"
	                                        else
	                                                echo "$device $label false" >> toMount.txt
	                                        fi
	                                else
	                                        echo "$device $label false" >> toMount.txt
	                                fi


				fi
			fi
		fi


	else
		fatalError "The device $filesystem has been labellised already"
	fi




	
else	#There is not a partition table



	#Check if it's already partitionned
        blkid /dev/$device | grep " TYPE=" >> stderr
        if [ $? -eq 0 ]
        then
	        #Check if it is a xfs partition
		xfs_admin -l /dev/$device >>stderr 2>&1

		if [ $? -eq 0 ] #The device is a xfs partition and has a label
        	then
                	#Check if the label is available
                	label=$(xfs_admin -l /dev/$device | sed 's/.\{9\}//'| sed -e 's/.\{1\}$//')

                	#Check if the label looks like "/data". If it's not the case, it stops the program.
                	if echo "$label" | grep -E '^/data' >/dev/null 2>&1
                	then
                        	echo "The device /dev/$device already has a label, which is $label" >> stderr

                        	#Check if the number is valid
                        	labelLength=${#label}
                        	if [[ $labelLength -lt 7 ]]
                        	then
                        	        fatalError "The device /dev/$device's label ($label) doesn't seems to match"
                        	fi

                        	#Check if this label is not already used by a mounted device
                        	mountlist=$(mount)
                        	if echo "$mountlist" | grep "$label" >/dev/null 2>&1
                        	then
                        	        fatalError "The label $label doesn't seems to be available"
                        	fi

                        	#The device still needs to be mounted with its label.   
                        	if [ -f toMount.txt ]
                        	then
                                	cond1=$(grep $device toMount.txt)
                                	cond2=$(grep $label toMount.txt)
                                	if [[ -n $cond1  || -n $cond2 ]]
                                	then
                                        	fatalError "Something went wrong. Please try again"
                                	else
                                        	echo "$device $label false" >> toMount.txt
                                	fi
                        	else
                                        echo "$device $label false" >> toMount.txt
                                fi

                        else
                                fatalError "The device /dev/$device already has a label, but it doesn't seems to be an available label"
                        fi
	else	#This device needs to be partitionned
 
		echo "The device is empty" >> stderr
		./fst-labellisation.sh $device
		if [ $? -ne 0 ]
		then
			fatalError "The labelisation of the device /dev/$device failed"
		fi	

        fi



		#It is already partitionned

			## Check if it is already mounted
	
			## Check if it's an xfs partition
			
			## Check if the label is available		

		#It's not
			
			## labellisation


fi


