#!/bin/bash

#The device name is taken as an argument : type ./script DeviceName (the device name must be written without its path)


#function which write an entire description of what gone wrong in stderr, prevent the user that there is a problem and quit the program. It take the lessage which is written is stderr as an argument.

fatalError () {
	echo "The program met an unexpected error and stop working. Please consult the error stream for more information. $1 "
	echo $1 >> stderr
	exit 1
}


#Mount a filesystem which already exist

device=$2
program=$(basename $0)
label=""

#Check that the packages needed are already installed

findfs=/sbin/findfs
[[ -x $findfs ]] || fatalError "No $findfs executable found. To solve this problem, please install e2fsprogs"

blkid=/sbin/blkid
[[ -x $blkid ]] || fatalError "No $blkid executable found, please install e2fsprogs"

e2label=/sbin/e2label
[[ -x $e2label ]] || fatalError "No $e2label executable found, please install e2fsprogs"


#Check if the argument is here

if [ -z $device ]
then 
	echo "Everything didn't work as planned. Please check the error stream for more information."
	fatalError "Error : : invalid argument. Please try again by running the command : ./$program --dev DeviceName"

fi

#Label check in

	#Check if the device has a label

A FAIRE : RECUPERER LE LABEL. UTILISER LA COMMANDE blkid -o list /dev/$device

label=$(blkid /dev/vdb | awk '{ print $2 }')
 
hasALabel=echo $label | grep -q "LABEL="; echo $?

if [$hasALabel -eq 1]
then 
	label= $(echo $label | cut -c8- | cut -d\" -f1)
	echo "This device already has a label, which is $label"
else 
	echo "Quand on crée un filesystem on met un label, connard !!!!!!"

fi

#--------------------------------------------------------------------------------------------------------

	#filesystem data recovery

blkid >> listDevice.txt

	#Use of these data


while read ligne

do
	# Reset of the variables which will be used
	unset LABEL UUID TYPE sdx autres

	# Recovery of the name of the device
	sdx=${ligne%%:*}

	# Deletion of the first value in the file
	autres=${ligne#* }

	# Recovery of LABEL, UUID, TYPE (LABEL is the one which interest us here)
	eval ${autres}

	# Creation of a file with every labels
	echo $LABEL >> labels.txt

done

#--------------------------------------------------------------------------------------------------------

#Creation of the mouting directory

mkdir /${label}

#Mounting

if (! -e label)
then

        mount -t xfs /dev/$device /$label

else
        echo "Erreur : Un point de montage a déjà été créé à partir de ce label" >> stderr
        echo "Le montage ne s'est pas passé comme prévu. Veuillez consulter la sortie erreur pour plus d'information."
        exit
fi

#eosfstregister Use

        #Insérer ici une fonction qui permette d'attendre que les tâches en écriture soit finies
/usr/sbin/eosfstregister $mountpoint spare:1

