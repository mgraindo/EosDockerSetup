#!/bin/bash

############################################
#     Check on the state of the device     #
############################################

#Fonctions which be used in the code

##Print an error message taken as argument and exit the program
fatalError () {
        echo "Error : $1" >> stderr
        exit 1
}

#--------------------------------------------------------------------------------------------------------

#Variables and default options

device=$1

opt_force=$2
opt_wipe=$3

#--------------------------------------------------------------------------------------------------------

echo "######################################" >> stderr
echo "# Preparation of the device /dev/$device #" >> stderr
echo "######################################" >> stderr

#--------------------------------------------------------------------------------------------------------

#Check if the filesystem is still mounted

if mount | grep -q "/dev/$device"
then
        if $opt_force
        then
                #Unmount filesystems
                umount /dev/$device
                if [ $? -ne 0 ]
                then
                        fatalError "The unmounting failed"
                fi
        else
                fatalError "data filesystem already mounted, use the --force option to unmount"
        fi
fi

#Wipe disks if requested

if $opt_wipe
then
        echo "Wiping device: /dev/$device" >> stderr
        eval "/bin/dd if=/dev/zero of=$device bs=1M count=8 2>/dev/null" || fatalError "The system met a problem during the device wiping"
        eval "/bin/sync" || fatalError "The system met a problem during the device wiping"
        echo "The device is totally clean" >> stderr
        echo "" >> stderr
fi

#------------------------------------------------------------------------------------------------------

#Check the state of the device

# New variable : a number which describe the state of the system
## 1 : Ready to be mounted
## 2 : Doesn't have a partition yet (partitioning needed) 

systemState="0"

        #Check if the device has already been partitioned, and if its type is "xfs"

j=$(blkid)


if echo "$j" | grep "/dev/$device" >/dev/null 2>&1
then

        i=$(blkid /dev/$device)

        if echo "$i" | grep 'TYPE=' >/dev/null 2>&1
        then
		if echo "$i" | grep 'TYPE=\"xfs\"' >/dev/null 2>&1
		then

                	systemState="1"
		else
			fatalError "The device /dev/$device is not a xfs partition."
		fi

        else

                echo "The device /dev/$device needs to be partitionned." >> stderr
		systemState="2"

        fi

else

        echo "The device /dev/$device needs to be partitionned." >> stderr
        systemState="2"

fi



case $systemState in

        #If the system is already partitionned :

        "1")
                #Check if there is an available label

                #TODO
		i=$b(blkid /dev/$device)

                if echo "$i" | grep " LABEL=" >/dev/null 2>&1
                then

                        #Access to the existing label
                        label=$(blkid /dev/$device | awk '{print $2}' | sed 's/.\{7\}//'| sed -e 's/.\{1\}$//')

                        #Check if the label looks like "/data". If it's not the case, it stops the program.
                        if echo "$label" | grep -E '^/data' >/dev/null 2>&1
                        then
                                echo "The device /dev/$device already has a label, which is $label" >> stderr

                                #Check if the number is valid
                                labelLength=${#label}
                                if [[ $labelLength -lt 7 ]]
                                then
                                        fatalError "The device /dev/$device already has a label, but it doesn't seems to be an available label"
                                fi

                                #Check if this label is not already used by a mounted device
                                mountlist=$(mount)
                                if echo "$mountlist" | grep "$label" >/dev/null 2>&1
                                thedn
                                        fatalError "A device is already mounted with the label $label"
                                fi
				
				#The device still needs to be mounted with its label.	
				if [ -f toMount.txt ]
				then
					cond1=$(grep $device toMount.txt)
					cond2=$(grep $label toMount.txt)
					if [[ -n $cond1  || -n $cond2 ]]
					then
						fatalError "Something went wrong. Please try again"
					else
						echo "$device $label false" >> toMount.txt 	
					fi	
				else		
					echo "$device $label false" >> toMount.txt
				fi

                        else
                                fatalError "The device /dev/$device already has a label, but it doesn't seems to be an available label"
                        fi

                else
                        fatalError "The device /dev/$device seems to be partitionned, but no label has been found."

                fi

        ;;


        #If the system isn't partitionned :

        "2")
                ./fst-labellisation.sh $device
                if [ $? -ne 0 ]
                then
                        fatalError "The labelisation of the device /dev/$device failed"
                fi
		
        ;;




        #Otherwise (what should normally not happened...)

        *)
                fatalError "The problem meet an unexpected error and stop working. Please try again."
        ;;

esac


