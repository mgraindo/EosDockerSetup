#!/bin/bash

#################################
#             main              #
#################################

# 2 ways of use : With a device passed in argument, what check and mount only this device, or without this argument, what check and mount every devices on the system.
# Use : ./eos-docker-fst-setup.sh [--dev $dev] [--force] [--wipe] [--not-register] [--help]

#Fonctions which be used in the code

##Print an error message taken as argument and exit the program
fatalError () {
        echo "Error : $1"
        echo $1 >> stderr
        exit 1
}

##Print the shape of the command
program=$(basename $0)

usage () {
        echo "Usage: $program [--dev <device>] [--force] [--wipe] [--not-register] [--help]"
}

##Print the entire descrption of the command options
help () {
        usage
        echo "    --dev <device>  device name, without /dev/, for example sdb"
        echo "    --force         unmount and relocate filesystems if necessary"
        echo "    --wipe          remove all existing filesystem data"
        echo "    --not-register  do not register the filesystem on EOS"
        echo " "
        echo "    the new filesystems will be mounted at /data<01-99>"
}


#-------------------------------------------------------------------------------------------------------

#Check that the packages needed are already installed

findfs=/sbin/findfs
[[ -x $findfs ]] || fatalError "No $findfs executable found. To solve this problem, please install e2fsprogs"

blkid=/sbin/blkid
[[ -x $blkid ]] || fatalError "No $blkid executable found, please install e2fsprogs"

xfstools=/usr/sbin/mkfs.xfs
[[ -x $xfstools ]] || fatalError "No $xfstools executable found, please install xfsprogs"


#-------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------


#Variables and default options

device=
program=$(basename $0)

opt_wipe=false
opt_force=false
opt_register=true
opt_loop=true

#--------------------------------------------------------------------------------------------------------

# Command-line parsing 

while [[ $# -gt 0 && $1 = --* ]] 
do
        case $1 in
                --dev)
                        device=$2
                        shift 2
                        opt_loop=false
                        ;;
                --force)
                        opt_force=true
                        shift
                        ;;
                --wipe)
                        opt_wipe=true
                        shift
                        ;;
                --not-register)
                        opt_register=false
                        shift
                        ;;
                --help)
                        help
                        exit
                        ;;
                *)
                        usage
                        fatalError "Unknown option: '$1'"
                        ;;
        esac
done


if $opt_loop
then

        ls /dev | grep "sd\|vd" >> devices.txt


	if $opt_wipe
	then
#			verif= ""
#			while [ $verif != yes || $verif != no ]
#			do
#				echo "You're about to wipe all your devices... Are you really sure ? [yes/no]"
#				read verif
#				case $verif in
#					yes)

					while read line
					do
						device=$line
						if $opt_force
						then
							if $opt_register
							then
								./eos-fst-setup.sh --dev $device --wipe --force
							else
								./eos-fst-setup.sh --dev $device --wipe --force --not-register
							fi
						else
							if $opt_register
							then
								./eos-fst-setup.sh --dev $device --wipe
							else
								./eos-fst-setup.sh --dev $device --wipe --not-register
							fi
						fi

					done < devices.txt

#				;;

#				no)
#					fatalError "Please try again"
#				;;
#
#			esac
#
#		done
	
	else
	
		while read line
		do
			device=$line
				if $opt_force
				then
					if $opt_register
					then
						./eos-fst-setup.sh --dev $device --force
					else
						./eos-fst-setup.sh --dev $device --force --not-register
					fi
				else
					if $opt_register
					then
						./eos-fst-setup.sh --dev $device
					else
						./eos-fst-setup.sh --dev $device --not-register
					fi
				fi
		done < devices.txt

	fi
	rm devices.txt

else
	
	if $opt_wipe
	then
		if $opt_force
		then
			if $opt_register
			then
				./eos-fst-setup.sh --dev $device --force --wipe
			else
				./eos-fst-setup.sh --dev $device --force --wipe --not-register
			fi
		else
			if $opt_register
                        then
                                ./eos-fst-setup.sh --dev $device --wipe
                        else
                                ./eos-fst-setup.sh --dev $device --wipe --not-register
                        fi
		fi	
	else
                if $opt_force
                then
                        if $opt_register
                        then
                                ./eos-fst-setup.sh --dev $device --force
                        else
                                ./eos-fst-setup.sh --dev $device --force --not-register
                        fi
                else
                        if $opt_register
                        then
                                ./eos-fst-setup.sh --dev $device 
                        else
                                ./eos-fst-setup.sh --dev $device --not-register
			fi
		fi
	fi
fi
