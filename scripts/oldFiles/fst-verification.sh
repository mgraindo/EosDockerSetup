#!/bin/bash


############################################
#     Check on the state of the device     #
############################################

#Fonctions which be used in the code

##Print an error message taken as argument and exit the program
fatalError () {
        echo "Error : $1" >> stderr
        exit 1
}

##Execute the command or stop the program if it fails
noisily () {
        local cmd="$*"
        echo "    $cmd" >>stderr
        eval "$cmd" || die "command failed: $cmd"
}

##Execute the command or print an error message if it fails
less_noisily () {
        local cmd="$*"
        echo "  $cmd" >> stderr
        eval "$cmd" || fatalError "command failed: $cmd"
}

##Execute the command without saying if it succeeded or failed
quietly () {
        local cmd="$*"
        echo "  $cmd" >> stderr
        eval "$cmd" || echo ""
}

#--------------------------------------------------------------------------------------------------------
 
#Variables and default options
 
device=$1
 
opt_force=$2
opt_wipe=$3

#--------------------------------------------------------------------------------------------------------
 
echo "######################################" >> stderr
echo "# Preparation of the device /dev/$device #" >> stderr
echo "######################################" >> stderr
 
#--------------------------------------------------------------------------------------------------------

# Check that the disk is not the one containing the OS
 
for dev in $( /bin/df | grep " /$\| /boot$\| /var$" | awk '{print $1}' | tr -d '1234567890' | uniq )
do
	if [[ $dev = "/dev/"$device ]]
        then
        	echo "You are trying to wipe, format and register the ROOT filesystem, are you really sure??" >> stderr
		
                break
	fi
done

 
#Check if the filesystem is already mounted
 
if mount | grep -q "/dev/$device"
then
        if $opt_force
        then
                #Unmount filesystems
                umount /dev/$device
                if [ $? -ne 0 ]
                then
                        fatalError "The unmounting of the filesystem $device failed"
                fi
        else
                fatalError "The data filesystem $device is already mounted, use the --force option to unmount"
        fi
fi

 
# Wipe disks if requested
 
if $opt_wipe
then
	echo "Wiping device: /dev/$device" >> stderr
        noisily "/bin/dd if=/dev/zero of=$device bs=1M count=8 2>/dev/null"
        noisily "/bin/sync"
        echo "$device is totally clean." >> stderr
        echo "" >> stderr
 
        # Actualisation of the devices list
        ls /dev | grep "sd\|vd" > devices.txt
fi
 

#------------------------------------------------------------------------------------------------------






xfs_admin -l /dev/$device >> stderr 2>&1

if [ $? -eq 0 ] #The device is a xfs partition and has a label
then
        #Check if the label is available
        label=$(xfs_admin -l /dev/$device | sed 's/.\{9\}//'| sed -e 's/.\{1\}$//')

        ##Check if the label looks like "/data". If it's not the case, it stops the program.
        if echo "$label" | grep -E '^/data' >/dev/null 2>&1
        then
                echo "The device /dev/$device already has a label, which is $label" >> stderr

                ##Check if the number is valid
                labelLength=${#label}
                if [[ $labelLength -lt 7 ]]
                then
                        fatalError "The device /dev/$device's label ($label) doesn't seems to match"
                fi

                ##Check if this label is not already used by a mounted device
                mountlist=$(mount)
                if echo "$mountlist" | grep "$label" >/dev/null 2>&1
                then
                        fatalError "The label $label doesn't seems to be available"
                fi
 
                ##Once everything's okay, the device still needs to be mounted with its label.   
                if [ -f toMount.txt ]
                then
                        cond1=$(grep $device toMount.txt)
                        cond2=$(grep $label toMount.txt)
                        if [[ -n $cond1  || -n $cond2 ]]
                        then
                                fatalError "Something went wrong. Please try again"
                        else
                                echo "$device $label false" >> toMount.txt
                        fi
                else
                        echo "$device $label false" >> toMount.txt
                fi
 
        else
                fatalError "The device /dev/$device already has a label, but it doesn't seems to be an available label"
        fi

else    #The device is empty or not usable
        blkid /dev/$device | grep 'TYPE=' >> stderr
        if [ $? -eq 0 ]
        then
                fatalError "The device $device can't be used"
        fi
                  

        # The device is empty and can be partitionned
        echo "The device $device is empty" >> stderr
	./fst-labellisation.sh $device
        if [ $? -ne 0 ]
        then
                fatalError "The labellisation of the device /dev/$device failed"
        fi

fi

