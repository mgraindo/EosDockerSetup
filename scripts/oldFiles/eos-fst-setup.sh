#!/bin/bash

##############################
#       Main program         #
##############################

program=$(basename $0)

# Use : ./filesystem.sh --dev $dev [--force] [--wipe] [--not-register] [--help]

#Fonctions which be used in the code

##Print an error message taken as argument and exit the program
fatalError () {
        echo "Error : $1"
        echo $1 >> stderr
        exit 1
}

##Print the shape of the command
usage () {
	echo "Usage: $program --dev <device> [--force] [--wipe] [--not-register] [--help]"
}

##Print the entire descrption of the command options
help () {
	usage
	echo "    --dev <device>  mandatory device name, for example /dev/sdb"
	echo "    --force         unmount and relocate filesystems if necessary"
 	echo "    --wipe          remove all existing filesystem data"
	echo " 	  --not-register  do not register the filesystem on EOS"
	echo " "
	echo "    the new filesystems will be mounted at /data<01-99>"
}

#--------------------------------------------------------------------------------------------------------

#Variables and default options

device=
program=$(basename $0)

opt_wipe=false 
opt_force=false 
opt_register=true
opt_stop=true

#--------------------------------------------------------------------------------------------------------
 
# Command-line parsing 
 
while [[ $# -gt 0 && $1 = --* ]] 
do
 	case $1 in
 		--dev)
 			device=$2
 			shift 2
 			opt_stop=false
 			;;
 		--force)
 			opt_force=true
			shift
 			;;
 		--wipe)
 			opt_wipe=true
 			shift
 			;;
 		--not-register)
 			opt_register=false
 			shift
 			;;
 		--help)
 			help
 			exit
 			;;
 		*)
 			usage
 			fatalError "Unknown option: '$1'"
 			;;
 	esac 
done 

#Check if the device name is given in the arguments and if the device exists

if $opt_stop
then
	usage
	fatalError "no device specified (--dev <device>)"
else

	#Check the existence of the device

	if  ls -a /dev | grep -q "$device" # Remplacement de fdisk au cas ou pb
	then
	        echo "######################################"
	        echo "#    Setup of the device /dev/$device    #"
	        echo "######################################"
	else
	        fatalError "This device doesn't seems to exist. Please try again."
	fi

fi


#Check if the filesystem is still mounted

if mount | grep -q "/dev/$device"
then
	if $opt_force
	then	
		#Unmount filesystems
		umount /dev/$device
		if [ $? -ne 0 ]
		then
			fatalError "The unmounting failed"
		fi
	else
		fatalError "data filesystem already mounted, use the --force option to unmount"
	fi
fi

#Wipe disks if requested

if $opt_wipe
then
	echo "Wiping device: $device"
	eval "/bin/dd if=/dev/zero of=$device bs=1M count=8 2>/dev/null" || fatalError "The system met a problem during the device wiping"
	eval "/bin/sync" || fatalError "The system met a problem during the device wiping"
	echo "The device is totally clean"
	echo ""
fi

#------------------------------------------------------------------------------------------------------------------------------------------

#Check the state of the device

# New variable : a number which describe the state of the system
## 1 : Ready to be mounted
## 2 : Doesn't have a partition yet (partitioning needed) 

systemState="0"

        #Check if the device has already been partitioned, and if its type is "xfs"

j=$(blkid)


if echo "$j" | grep "/dev/$device" >/dev/null 2>&1
then
        i=$(blkid /dev/$device)
	
        if echo "$i" | grep 'TYPE=\"xfs\"' >/dev/null 2>&1
        then
                echo "Your filesystem is ready to be mounted"

                systemState="1"

        else
                fatalError "The device /dev/$device is not a xfs partition."
        fi

else 

        echo "Your system needs to be partitionned."

        systemState="2"
	

fi


        #If the system is already partitionned :

case $systemState in




        #If the system is already partitionned :

        "1")
                #Check if there is an available label

                i=$(blkid /dev/$device)

                if echo "$i" | grep "LABEL=" >/dev/null 2>&1
                then

                        #Access to the existing label
			label=$(blkid /dev/$device | awk '{print $2}' | sed 's/.\{7\}//'| sed -e 's/.\{1\}$//')
  
                        #Check if the label looks like "/data". If it's not the case, it stops the program.
 
			if echo "$label" | grep "/data" >/dev/null 2>&1
                        then
                                echo "This device already has a label, which is $label"

                                #Check if the number is valid
				labelLength=${#label}
                                if [[ $labelLength -lt 7 ]]
				then
					fatalError "This filesystem already has a label, but it doesn't seems to be an available label. To avoid any bad manipulation, the program will stop now."
				fi

				#Check if this label is not already used by a mounted device
				mountlist=$(mount)
				if echo "$mountlist" | grep "$label" >/dev/null 2>&1
				then
					fatalError "A device is already mounted with this label"
				fi
	
                        else
                                fatalError "This filesystem already has a label, but it doesn't seems to be an available label. To avoid any bad manipulation, the program will stop now."
                        fi

                else
                        fatalError "Your device seems to be partitionned, but no label has been found."

                fi

        ;;




        #If the system isn't partitionned :

        "2")
                ./fst-creation.sh $device
        ;;





        #Otherwise (what should normally not happened...)

        *)
                fatalError "The problem meet an unexpected error and stop working. Please try again."
        ;;

esac

#Normally the filesystem is created and everything has been checked before

./fst-mounting.sh $device


#-----------------------------------------------------------------------------------------------------------------------------------------

if $opt_register
then

	###################################
	#         Registering             # 
	###################################

	## FIX ME : Insérer ici une fonction qui permette d'attendre que les tâches en écriture soit finies
	eosfstregister $label spare:1  
	
	#Check if eosfsid is assigned

	if [[ -e "$label/.eosfsuuid" ]]
	then
		echo "FSUUID assigned to $label"
		ls -la $mount_point/.eosfsuuid
		ls -la $mount_point/.eosfsid
	fi

fi                                   
