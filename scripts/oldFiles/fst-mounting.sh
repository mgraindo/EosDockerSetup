#!/bin/bash


#Mount a filesystem which already exist
#The device name is taken as an argument : type ./script DeviceName (the device name must be written without its path)

#--------------------------------------------------------------------------------------------------------

#function which prevent the user that there is a problem and quit the program. It take the error message as an argument.

fatalError () {
        echo "Error : $1"
        echo $1 >> stderr
        exit 1
}


###################################
#           Mounting              #
###################################


#--------------------------------------------------------------------------------------------------------

#variables

device=$1
program=$(basename $0)
label=$(blkid /dev/$device | awk '{print $2}' | sed 's/.\{7\}//'| sed -e 's/.\{1\}$//')

#-------------------------------------------------------------------------------------------------------

#Check if a directory named with this label already exists, and if it can be a mountpoint
if [ -d $label ]
then
	i=$(ls -A $label)i
	if [ -z "$i" ]
	then
	        fatalError "A non empty directory $label was already created. Please clean this directory and try again"
	fi
else
	#Creation of the mouting directory if it doesn't exist
	mkdir "$label"
	if [ $? -ne 0 ]
	then
        	fatalError "The system was not able to create the mountpoint directory"
	fi
fi

#--------------------------------------------------------------------------------------------------------
 
#Mounting
mount -t xfs /dev/$device $label 
if [ $? -ne 0 ]
then
	fatalError "The mounting failed"
fi

#--------------------------------------------------------------------------------------------------------

#Check if the system is well mounted
		
#mount >> verif.txt


#if grep -q "/dev/$device" verif.txt
#then
#	echo "The device has been successfully mounted. The mountpoint is $label."
#else 
#	rm verif.txt
#	fatalError "Something gone wrong during the mounting, please try again."
#fi		

#rm verif.txt

exit 0
