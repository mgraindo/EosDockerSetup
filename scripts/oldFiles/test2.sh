#!/bin/bash


#######################################
#        Filesystem creation          #
#######################################



#The device name is taken as an argument : type ./script DeviceName (the device name must be written without its path)


#function which write an entire description of what gone wrong in stderr, prevent the user that there is a problem and quit the program. It take the lessage which is written is stderr as an argument.

fatalError () {
        echo "The program met an unexpected error and stop working. Please consult the error stream for more information. $1"
        echo $1 >> stderr
        exit 1
}

device=$1
program=$(basename $0)
label=""



#Check that the packages needed are already installed

blkid=/sbin/blkid
[[ -x $blkid ]] || fatalError "No $blkid executable found, please install e2fsprogs"

xfstools=/usr/sbin/xfs_admin
[[ -x $xfstools ]] || fatalError "No $xfstools executable found, please install xfsprogs"

#-------------------------------------------------------------------------------------------------------

#Check if the argument is here

if [ -z $device ]
then
        echo "Everything didn't work as planned. Please check the error stream for more information."
        fatalError "Error : : invalid argument. Please try again by running the command : ./$program --dev DeviceName"

fi

#-------------------------------------------------------------------------------------------------------

#Label check in (use of the command blkid, the output is put in a variable. Check if the second column of the file contain "LABEL=". If so, there is a label, if not, there is not and the program stop there.)

i=$(blkid /dev/$device | awk '{print $2}')
if echo "$i" | grep "LABEL=" >/dev/null 2>&1

then
        #Access to the existing label
        label=$(blkid /dev/$device | awk '{print $2}' | sed 's/.\{7\}//'| sed -e 's/.\{1\}$//')

        #Check if the label isn't empty. If it's the case, it stops the program.
        if [ -n $label ]
        then
                echo "This device already has a label, which is $label"
		exit
        else
                echo "Error : this filesystem already has a label, but it seems to be an empty label. To avoid any bad manipulation, the program will stop now."
                #Idea if time : add a proposal to reformate the filesystem with a real label       
        fi

else
        echo "No label has been found, one will be generated right now"

#------------------------------------------------------------------------------

        #Automatic generation of labels.
        ##FIX ME :  If a number is not used it's supposed to be choosed as the label number.

        #filesystem data recovery

        blkid > listDevice.txt

        #Use of these data

        while read line

        do

                # Reset of the variables which will be used
                unset LABEL UUID TYPE sdx autres

                # Recovery of the name of the device
                sdx=${line%%:*}

                # Deletion of the first value in the file
                others=${line#* }

                # Recovery of LABEL, UUID, TYPE (LABEL is the one which interest us here)
                eval ${others}

                # Creation of a file with every labels
                echo $LABEL >> labels.txt

        done < listDevice.txt

        while read line
        do

                # Reset of the "number" variable
                unset number
                                                                                      
                # Deletion of the first part of the labels and creation of a file with every label number
                
               echo  $line | sed 's/.\{5\}//' >> labelNumbers.txt

        done < labels.txt

        sort -n  labelNumbers.txt #The max number is now the one on the last line of the file

	#Deletion of empty lines in th labelNumbers.txt file
	sed -i '/^$/d' labelNumbers.txt

        #Recovery of the max number
        max=$(tail -n 1 labelNumbers.txt)
        echo $max

        #Check if there is some free label numbers before the max one
        #nbLabel=$(wc -l labelNumbers.txt)
        lines=$(wc -l labelNumbers.txt | awk '{ print $1 }')
        echo $lines

        #Check : if the number of labels is equal to the max, there is not empty label. Else, we can take one of them.
        if [ $lines -eq $max ]
        then
                #No empty label before the last one
                let "newLabelPostfixNumber = $max + 1"

                #Creation of the new label
                if [ $newLabelPostfixNumber -le 9 ]
                then
                        newLabelPostfix="0"$newLabelPostfixNumber
                else
                        newLabelPostfix=$newLabelPostfixNumber
                fi

                echo "Aucun numero de label ne s'est libéré. Le numero du label sera donc : $newLabelPostfix"

        else

                #Loop to see if there is a free label before the last one

                ##Define the variables which will be needed in the loop
                let "tmp = 1"
                newLabelPostfix="0"$tmp

                ##Loop
                while read line && [[ $newLabelPostfix = $line ]]
                do
                                let "tmp = $tmp + 1"
                                if [ $tmp -le 9 ]
                                then
                                        newLabelPostfix="0"$tmp
                                else
                                        newLabelPostfix=$tmp
                                fi


                done < labelNumbers.txt

                echo "Ce numéro de label est libre : $newLabelPostfix"

        fi

        newLabel="/data"$newLabelPostfix

        echo "Voici le nouveau label : $newLabel"


        #Deletion of the files created during the operation.

        rm listDevice.txt
        rm labels.txt
        rm labelNumbers.txt

        
        echo $newLabelPostfix
        echo $newLabelPrefix

        echo "Voilà le nouveau label obtenu"
        echo $newLabel


fi

#-------------------------------------------------------------------------------------------------------

# Partitionning of the device

mkfs.xfs -L $newLabel /dev/$device

# Check if the partitionning is well done : The type of the partition must be "xfs". No need to check if the label exists, it was done before.

i=$(blkid /dev/$device | awk '{print $4}')
if echo "$i" | grep 'TYPE=\"xfs\"' >/dev/null 2>&1
then
	echo "Your filesystem is ready to be mounted"
else
	fatalError "Something went wrong during the partitionning. The program will stop now, please try again."
fi


