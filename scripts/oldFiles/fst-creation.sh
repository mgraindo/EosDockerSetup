#!/bin/bash


#######################################
#        Filesystem creation          #
#######################################


#The device name is taken as an argument : type ./script DeviceName (the device name must be written without /dev/)


#function which prevent the user that there is a problem and quit the program. It take the message which is written as an argument.

fatalError () {
        echo "Error : $1"
        echo $1 >> stderr
        exit 1
}

#--------------------------------------------------------------------------------------------------------

#Variables
device=$1
label=""

#--------------------------------------------------------------------------------------------------------

#Automatic generation of labels.

#filesystem data recovery
blkid | grep "/data" > listDevice.txt

#Use of these data : Creation of a file with the labels of all the device's filesystems and another one with there numbers. 

while read line

do

	# Reset of the variables which will be used
	unset LABEL UUID TYPE sdx autres

	# Recovery of the name of the device
	sdx=${line%%:*}

	# Deletion of the first value in the file
        others=${line#* }

        # Recovery of LABEL, UUID, TYPE (LABEL is the one which interest us here)
        eval ${others}

        # Creation of a file with every labels
        echo $LABEL >> labels.txt

done < listDevice.txt

while read line
do

	# Reset of the "number" variable
        unset number
                                                                                      
        # Deletion of the first part of the labels and creation of a file with every label number

        echo  $line | sed 's/.\{5\}//' >> labelNumbers.txt

done < labels.txt

#--------------------------------------------------------------------------------------------------------
# Check which label is free from the numbers file.

sort -n labelNumbers.txt >> labelNumbersSorted.txt #The max number is now on the last line of the file

#Deletion of empty lines in th labelNumbers.txt file
sed -i '/^$/d' labelNumbersSorted.txt

#Recovery of the max number
max=$(tail -n 1 labelNumbersSorted.txt) >/dev/null 2>&1


# Check if there is some free label numbers before the max one
lines=$(wc -l labelNumbersSorted.txt | awk '{ print $1 }')

#Check : if the number of labels is equal to the max, there is not empty label. Else, we can take one of them.

if [ $lines -eq $max ] 
then

        #No empty label before the last one
	let "newLabelPostfixNumber = $max + 1"

        #Creation of the new label
        if [ $newLabelPostfixNumber -le 9 ]
        then
                newLabelPostfix="0"$newLabelPostfixNumber
        else
                newLabelPostfix=$newLabelPostfixNumber
        fi

else

        #Loop to see if there is a free label before the last one

        ##Define the variables which will be needed in the loop

	let "tmp = 1"
        newLabelPostfix="0"$tmp

        ##Loop
        while read line && [[ $newLabelPostfix = $line ]]
        do
	        let "tmp = $tmp + 1"
                if [ $tmp -le 9 ]
                then
        		newLabelPostfix="0"$tmp
                else
                        newLabelPostfix=$tmp
                fi		
	done < labelNumbersSorted.txt

fi

        newLabel="/data"$newLabelPostfix

        #Deletion of the files created during the operation.

        rm listDevice.txt
        rm labels.txt
        rm labelNumbers.txt
	rm labelNumbersSorted.txt

        echo "Your filesystem label is : $newLabel"

#-------------------------------------------------------------------------------------------------------

# Partitionning of the device

mkfs.xfs -L $newLabel /dev/$device
if [ $? -ne 0 ]
then
        fatalError "The system met a problem during the filesystem creation"
fi


# Check if the partitionning is well done : The type of the partition must be "xfs". No need to check if the label exists, it was done before.

i=$(blkid /dev/$device | awk '{print $4}')

if echo "$i" | grep 'TYPE=\"xfs\"' >/dev/null 2>&1
then
	echo "Your filesystem is ready to be mounted"
else
	fatalError "Something went wrong during the partitionning. The program will stop now, please try again."
fi

exit 0
