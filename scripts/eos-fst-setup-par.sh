#!/bin/bash


#####################
#   Main program    #
#####################


# 2 ways of use : With a device passed in argument, what check and mount only this device, or without this argument, what check and mount every devices on the system.
# Use : ./eos-docker-fst-setup.sh [--dev $dev] [--force] [--wipe] [--not-register] [--help]

#-------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------

#Fonctions which be used in the code

##Print an error message taken as argument and exit the program
fatalError () {
        echo "Error : Please check stderr for more informations"
        echo "Error : $1" >> stderr
        exit 1
}

##Execute the command or stop the program if it fails
noisily () {
        local cmd="$*"
        echo "    $cmd" >> stderr
        eval "$cmd" || fatalError "command failed: $cmd"
}
 
##Execute the command or print an error message if it fails
less_noisily () {
        local cmd="$*"
        echo "  $cmd" >> stderr
        eval "$cmd" || fatalError "command failed: $cmd"
}
 
##Execute the command without saying if it succeeded or failed
quietly () {
        local cmd="$*"
        echo "  $cmd" >> stderr
        eval "$cmd" || echo ""
}

##Print the shape of the command
program=$(basename "$0")

usage () {
        echo "Usage: $program [--dev <device>] [--force] [--wipe] [--not-register] [--help]"
}

##Print the entire description of the command options
help () {
        usage
        echo "    --dev <device>  device name, for example /dev/sdb"
        echo "    --force         unmount and relocate filesystems if necessary"
        echo "    --wipe          remove all existing filesystem data"
        echo "    --not-register  do not register the filesystem on EOS"
        echo " "
        echo "    the new filesystems will be mounted at /data<01-99>"
}

## Check if the device has partitions of the type fd and ee
device_partitions () {
        sfdisk --Linux --dump "$1" 2>/dev/null | awk '/Id=(83|fd|ee)/ {print $1}' | xargs
}

#-------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------

#Variables and default options

opt_wipe=false
opt_force=false
opt_register=true
opt_loop=true

#--------------------------------------------------------------------------------------------------------

# Command-line parsing 

while [[ $# -gt 0 && $1 = --* ]] 
do
        case $1 in
                --dev)
                        device=$2
                        shift 2
                        opt_loop=false
                        ;;
                --force)
                        opt_force=true
                        shift
                        ;;
                --wipe)
                        opt_wipe=true
                        shift
                        ;;
                --not-register)
                        opt_register=false
                        shift
                        ;;
                --help)
                        help
                        exit
                        ;;
                *)
                        usage
                        fatalError "Unknown option: '$1'"
                        ;;
        esac
done

#--------------------------------------------------------------------------------------------------------

#Creation of the files which will be used 

##List of the devices

os=$(blkid | grep -v "xfs" | grep -E ' TYPE' | cut -d : -f1 | sed 's/[0-9]//' | uniq)

DEV="/dev/"

for file in $DEV*
do
	if echo "$file" | grep "sd\\|vd" >> /dev/null
	then
                # Check that the disk is not the one containing the OS 
                if [ "$os" == "$file" ]
                then
                	echo "The OS is the device $file" >> stderr
                        echo "$file" >> verified.txt
                        partitions=$(sfdisk --Linux --dump "$file" 2>/dev/null)
			echo "$partitions" >> verified.txt
                else
                        grep -q "$file" verified.txt
                        if [ $? -ne 0 ]
                        then
                        echo "$file" >> devices.txt
                        	partitions=$(device_partitions "$file")
				if [[ -n $partitions ]]
				then
                                	echo "$partitions" >> verified.txt
				fi
                	fi
                fi
	fi
done

##For the labellisation

###filesystem data recovery
if [ $opt_wipe == false ]
then
	blkid | grep "/data" | awk '{print $1}' | sed 's/:*$//g' > listDevice.txt
fi

###Use of these data : Creation of a file with the labels of all the device's filesystems and another one with their numbers. 

if [[ -s listDevice.txt ]]
then
	while read line
	do

	        # Deletion of the first part of the labels and creation of a file with every label number

	        number=$(xfs_admin -l "$line" | sed 's/label = "\/data\(.*\)"$/\1/') >> stderr 2>&1
		numberLength=${#number}
		if [ "$numberLength" -ge 2 ]
		then
			echo "$number" >> labelNumbers.txt
		fi

	done < listDevice.txt

        rm listDevice.txt
        if [ $? -ne 0 ]
        then
                echo "Error : The system failed to remove the file listDevice.txt. Please do it yourself" >> stderr
        fi

fi

##For the partitionning and the mounting
touch toMount.txt


#-------------------------------------------------------------------------------------------------------------------------------------------

if $opt_loop
then

	#Device preparation (sequential part)

	while read line 
	do

		device=$line

		{
		echo "######################################"
		echo "# Preparation of the device $device #"
		echo "######################################"
		} >> stderr

		hasPartition=false
                partitions=$(device_partitions "$device")

                if [[ -n $partitions ]]
                then
                        grep -qE "$device"$ verified.txt
                        if [ $? -ne 0 ]
                        then
                                filesystem="$device""1"
				hasPartition=true
				echo "$device" >> verified.txt
                        else
                                echo "The device $device has already been verified" >> stderr
				continue
                        fi
		else
                        grep -q "$device" verified.txt
                        if [ $? -ne 0 ]
                        then
                                filesystem="$device"
				echo "$device" >> verified.txt
                        else
                                echo "The device $device has already been verified" >> stderr
				continue
                        fi

                fi

		#Check if the filesystem is already mounted

		isMounted=false #By default a device is not mounted

		if mount | grep "$filesystem" 
		then
			# Do something only if the --force option is used
        		if $opt_force
        		then
                		#Unmount filesystem
				label=$(mount | grep "$filesystem" | awk '{print $3}')
                		umount "$label"
                		if [ $? -ne 0 ]
                		then
               			        fatalError "The unmounting of the filesystem $filesystem failed"
                		fi
        		else
				# Skip this device
                		echo "The data filesystem $filesystem is already mounted, use the --force option to unmount" >> stderr
				isMounted=true
			fi
		fi

		if $isMounted
		then
			continue
		fi


		# Wipe disks if requested

		if $opt_wipe
		then
			if [ $hasPartition = false ]
			then
				echo "Wiping device: $device" >> stderr 
				noisily "/bin/dd if=/dev/zero of=$device bs=1M count=8 2>/dev/null"
				noisily "/bin/sync"
				echo "$device is totally clean." >> stderr
				echo "" >> stderr
			fi
		fi

		# Check if the device need to be partitionned

		./fst-verifications.sh "$filesystem"

	done < devices.txt

	# Deletion of temporary files which are not used any more

	if [ -e labelNumbers.txt ]
	then
        	rm labelNumbers.txt
        	if [ $? -ne 0 ]
        	then
        	        echo "Error : The system failed to remove the file listDevice.txt. Please do it yourself" >> stderr
        	fi
	fi
	if [ -e labelNumbersSorted.txt ]
	then
	        rm labelNumbersSorted.txt
	        if [ $? -ne 0 ]
	        then
	                echo "Error : The system failed to remove the file labelNumbersSorted.txt. Please do it yourself" >> stderr
	        fi
	fi
	if [ -e devices.txt ]
	then
	        rm devices.txt
	        if [ $? -ne 0 ]
	        then
	                echo "Error : The system failed to remove the file devices.txt. Please do it yourself" >> stderr
	        fi
	fi
	if [ -e verified.txt ]
	then
	        rm verified.txt
	        if [ $? -ne 0 ]
	        then
	                echo "Error : The system failed to remove the file verified.txt. Please do it yourself" >> stderr
	        fi
	fi



        #Partitionning and mounting (parallele part)

        while read line
        do
                device=$(echo "$line" | cut -d" " -f1)
                label=$(echo "$line" | cut -d" " -f2)  
		needPartitionning=$(echo "$line" | cut -d" " -f3)

                ./fst-partitionning-and-mounting.sh "$device" "$label" "$needPartitionning" "$opt_register" &

        done < toMount.txt

        wait

	if [ -e toMount.txt ]
	then
        	rm toMount.txt
        	if [ $? -ne 0 ]
        	then
                	echo "Error : The system failed to remove the file toMount.txt. Please do it yourself" >> stderr
		fi
        fi


else

        #Check the existence of the device

        if  grep -q "$device" devices.txt
        then
{
                echo "######################################"
                echo "#    Setup of the device /dev/$device    #"
                echo "######################################"
} >> stderr

        else
                fatalError "This device doesn't seems to exist or to be available. Please try again."
        fi

	#Determine the filesystem

	partitions=$(device_partitions "$device")

        if [[ -n $partitions ]]
        then
               	filesystem="$device""1" 
        else
               	filesystem="$device"
        fi


        #Check if the filesystem is already mounted

        if mount | grep "$filesystem"
        then
        	if $opt_force
                then
                	#Unmount filesystem
                        umount "$filesystem"
                        if [ $? -ne 0 ]
                        then
                        fatalError "The unmounting of the filesystem $filesystem failed"
                        fi
                else
                        fatalError "The data filesystem $filesystem is already mounted, use the --force option to unmount"
                fi
        fi


        if $opt_wipe
        then
                echo "Wiping device: $device" >> stderr
                noisily "/bin/dd if=/dev/zero of=$device bs=1M count=8 2>/dev/null"
                noisily "/bin/sync"
                echo "$device is totally clean." >> stderr
        	echo "" >> stderr
	fi

	# Check if the system needs to be partitionned
	./fst-verifications.sh "$filesystem"


	#Deletion of temporary files not used any more
	if [ -e labelNumbers.txt ]
	then
	        rm labelNumbers.txt
	        if [ $? -ne 0 ]
	        then
	                echo "Error : The system failed to remove the file listDevice.txt. Please do it yourself" >> stderr
	        fi
	fi
	if [ -e labelNumbersSorted.txt ]
	then
	        rm labelNumbersSorted.txt
	        if [ $? -ne 0 ]
	        then
	                echo "Error : The system failed to remove the file labelNumbersSorted.txt. Please do it yourself" >> stderr
	        fi
	fi
	if [ -e devices.txt ]
	then
        	rm devices.txt
        	if [ $? -ne 0 ]
	        then
	                echo "Error : The system failed to remove the file devices.txt. Please do it yourself" >> stderr
	        fi
	fi
	if [ -e verified.txt ]
	then
	        rm verified.txt
	        if [ $? -ne 0 ]
	        then
	                echo "Error : The system failed to remove the file verified.txt. Please do it yourself" >> stderr
	        fi
	fi

	# Mount the filesystem
	./fst-partitionning-and-mounting.sh "$filesystem"

        if [ $? -ne 0 ]
        then
                fatalError "The system failed"
        fi

	if [ -e toMount.txt ]
	then
	        rm toMount.txt
	        if [ $? -ne 0 ]
	        then
	                echo "Error : The system failed to remove the file toMount.txt. Please do it yourself" >> stderr
	        fi
	fi

	
fi

